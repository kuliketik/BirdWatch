#include <pebble.h>

static Window *window;
static TextLayer *s_time_layer;
static GFont *s_time_font;
static BitmapLayer *s_background_layer;
static GBitmap *s_background_bitmap;
static GBitmap *s_background_bitmap2;
static GFont *s_day_font;
static TextLayer *s_day_layer;
static bool flag;

static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  // Create GBitmap, then set to created BitmapLayer
  s_background_bitmap = gbitmap_create_with_resource(RESOURCE_ID_BIRD_BG_1);
  s_background_bitmap2 = gbitmap_create_with_resource(RESOURCE_ID_BIRD_BG_2);
  s_background_layer = bitmap_layer_create(GRect(0, 0, 144, 168));
  bitmap_layer_set_bitmap(s_background_layer, s_background_bitmap);
  flag=true;
  layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(s_background_layer));

  // Create time TextLayer
  s_time_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_PERFECT_DOS_48));
  s_time_layer = text_layer_create(GRect(0, 110, 144, 50));
  text_layer_set_background_color(s_time_layer, GColorClear);
  text_layer_set_text_color(s_time_layer, GColorBlack);
  text_layer_set_text(s_time_layer, "00:00");

  text_layer_set_font(s_time_layer, s_time_font);
  text_layer_set_text_alignment(s_time_layer, GTextAlignmentCenter);

  layer_add_child(window_layer, text_layer_get_layer(s_time_layer));

  // Create day TextLayer
  s_day_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_PERFECT_DOS_24));
  s_day_layer = text_layer_create(GRect(0, 0, 144, 25));
  text_layer_set_background_color(s_day_layer, GColorClear);
  text_layer_set_text_color(s_day_layer, GColorBlack);
  text_layer_set_text(s_day_layer, "Mon 01 01");

  text_layer_set_font(s_day_layer, s_day_font);
  text_layer_set_text_alignment(s_day_layer, GTextAlignmentCenter);

  layer_add_child(window_layer, text_layer_get_layer(s_day_layer));
}

static void update_time() {
    // Get a tm structure
    time_t temp = time(NULL);
    struct tm *tick_time = localtime(&temp);

    // Create a long-lived buffer
    static char buffer[] = "00:00";

    // Write the current hours and minutes into the buffer
    if(clock_is_24h_style() == true) {
        // Use 24 hour format
        strftime(buffer, sizeof("00:00"), "%H:%M", tick_time);
    } else {
        // Use 12 hour format
        strftime(buffer, sizeof("00:00"), "%I:%M", tick_time);
    }

    // Display this time on the TextLayer
    text_layer_set_text(s_time_layer, buffer);
}

static void update_day() {
    // Get a tm structure
    time_t temp = time(NULL);
    struct tm *tick_time = localtime(&temp);

    // Create a long-lived buffer
    static char buffer[] = "Mon 01 01";

    // Write the current hours and minutes into the buffer

    strftime(buffer, sizeof("Mon 01 01"), "%a %d %m", tick_time);

    // Display this time on the TextLayer
    text_layer_set_text(s_day_layer, buffer);
}

static void update_background() {
    if(!flag) {
        bitmap_layer_set_bitmap(s_background_layer, s_background_bitmap);
    } else {
        bitmap_layer_set_bitmap(s_background_layer, s_background_bitmap2);
    }
    flag = !flag;
}
static void tick_handler(struct tm *tick_time, TimeUnits units_changed) {
    update_day();
    update_time();
    update_background();
}

static void window_unload(Window *window) {
  // Destroy text layer
  text_layer_destroy(s_time_layer);

  // Destroy font
  fonts_unload_custom_font(s_time_font);
  fonts_unload_custom_font(s_day_font);

  // Destroy GBitmap
  gbitmap_destroy(s_background_bitmap);

  // Destroy BitmapLayer
  bitmap_layer_destroy(s_background_layer);
}

static void init(void) {
  window = window_create();

  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    .unload = window_unload,
  });
  const bool animated = true;
  window_stack_push(window, animated);

  // Register with TickTimerService
  tick_timer_service_subscribe(SECOND_UNIT, tick_handler);

  update_time();
}

static void deinit(void) {
  window_destroy(window);
}

int main(void) {
  init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", window);

  app_event_loop();
  deinit();
}
